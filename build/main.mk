# Just a snippet to stop executing under other make(1) commands
# that won't understand these lines
ifneq (,)
This makefile requires GNU Make.
endif

# By default compile in quite mode
# show the compilation commands if 
# V=1 option specified while compiling
ifeq ($(V),1)
quiet =
Q =
else
quiet=quiet_
Q = @
endif

BSP_PATH ?= stm32_l4_bsp
BUILD_DIR ?= out
TARGET_EXECUTABLE ?= $(notdir $(CURDIR))
BIN_FMT ?= bin
CC=${CROSS_COMPILE}gcc
OBJCOPY:=${CROSS_COMPILE}objcopy

# check for needed variables
ifeq ($(BSP_SRC_FILES),)
$(error "BSP_SRC_FILES variable not defined please define")
endif

ifeq ($(RTOS_SRC_FILES),)
$(info "RTOS_SRC_FILES variable not defined ignore if not using rtos")
endif

ifeq ($(APP_SRC_FILES),)
$(error "APP_SRC_FILES variable not define please define")
endif

ifeq ($(LDSCRIPT),)
$(error "LDSCRIPT Variable not defined ")
endif

ifeq ($(BSP_INC_PATHS),)
$(error "BSP_INC_PATHS Variable not defined ")
endif

# Default file list generation
BSP_SRC_FILES_WITH_PREFIX=$(addprefix $(BSP_PATH)/,$(BSP_SRC_FILES))
RTOS_SRC_FILES_WITH_PREFIX=$(addprefix $(BSP_PATH)/,$(RTOS_SRC_FILES))


SRC_FILE_WITH_PREFIX= $(BSP_SRC_FILES_WITH_PREFIX) \
		      $(APP_SRC_FILES)

OBJ_FILE_WITH_PREFIX=$(addsuffix .o,$(addprefix ${BUILD_DIR}/,$(basename $(SRC_FILE_WITH_PREFIX))))
INCLUDE_FLAGS=-I. $(BSP_INC_PATHS)

ARCH_FLAGS ?=-mcpu=cortex-m4 \
	     -mfloat-abi=hard \
	     -mfpu=fpv4-sp-d16 \
	     -mthumb 

# CFLAGS
CFLAGS ?= $(INCLUDE_FLAGS) \
	  $(CFLAGS_EXTRA) \
	$(ARCH_FLAGS) \
	-DCPU_MCIMX7D_M4 \
	-DNDEBUG \
	-Wall  \
	-fno-common  \
	-ffunction-sections  \
	-fdata-sections  \
	-ffreestanding  \
	-fno-builtin  \
	-mapcs  \
	-std=gnu99

# Linker FLAGS
LDLIBS ?=-lm
LINKER_FLAGS ?= -Xlinker -Map=$(TARGET_EXECUTABLE).map \
		-Xlinker --gc-sections  \
		-Xlinker -static \
		-Xlinker -z  \
		-Xlinker muldefs
OFLAGS ?= -Os
MISCFLAGS ?= \
	     --specs=nano.specs \
	     -mapcs

ifneq ($(LDSCRIPT),)
LDSCIRPT_FLAGS = -T $(LDSCRIPT) 
endif

LDFLAGS ?= $(ARCH_FLAGS) \
	   $(MISCFLAGS) \
	   $(LDLIBS) \
	   $(LDSCIRPT_FLAGS) \
	   -Wall  -fno-common \
	   -ffunction-sections  \
	   -fdata-sections  \
	   -ffreestanding  \
	   -fno-builtin 

$(TARGET_EXECUTABLE).bin: $(TARGET_EXECUTABLE).elf
	$(Q)echo "building $(@)"
	$(Q)$(OBJCOPY) -Obinary $(<) $(@) 

$(TARGET_EXECUTABLE).elf: ${OBJ_FILE_WITH_PREFIX}
	$(Q)$(CC) \
		-Wall \
		$(LDFLAGS) \
		-static $(LINKER_FLAGS) $(^) \
		-Wl,--start-group -lm -lc -lgcc \
		-lnosys -Wl,--end-group  -o $(@) $(LDLIBS)

$(BUILD_DIR)/%.o: %.c
	$(Q)echo "building $@"
	$(Q)mkdir -p $(@D)
	$(Q)$(CC) $(CFLAGS) -c $< -o $@

$(BUILD_DIR)/%.o: %.S
	$(Q)echo "building $@"
	$(Q)mkdir -p $(@D)
	$(Q)$(CC) $(C_FLAGS) -c $< -o $@


$(BUILD_DIR)/%.o: %.s
	$(Q)echo "building $@"
	$(Q)mkdir -p $(@D)
	$(Q)$(CC) $(C_FLAGS) -c $< -o $@


.PHONY: clean
clean:
	$(Q)echo "cleaning ..."
	-$(Q)rm -rvf $(BUILD_DIR)
	-$(Q)rm -f $(TARGET_EXECUTABLE).$(BIN_FMT)
	-$(Q)rm -f *.elf
	-$(Q)rm -f *.bin
