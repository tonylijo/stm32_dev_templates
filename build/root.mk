CROSS_COMPILE = /home/tony/toolchains/gcc-arm-none-eabi-5_4-2016q2/bin/arm-none-eabi-

LDSCRIPT=$(CURDIR)/app/linker/STM32L476VG_FLASH.ld

# Add the BSP INCLUDE PATHS HERE
BSP_INC_PATHS= \
	       -I$(BSP_PATH)/CMSIS/Device/ST/STM32L4xx/Include/ \
	       -I$(BSP_PATH)/CMSIS/Include/ \
	       -I$(BSP_PATH)/STM32L4xx_HAL_Driver/Inc/ \
	       -I$(BSP_PATH)/BSP/STM32L476G-Discovery/ \
	       -I$(BSP_PATH)/platform/CMSIS/Include/ \
	       -I$(CURDIR)/app/inc

CFLAGS_EXTRA= -DSTM32L476xx \
	-g
# Add RTOS SOURCE FILES HERE
#RTOS_SRC_FILES= \
		rtos/FreeRTOS/Source/tasks.c \
		rtos/FreeRTOS/Source/portable/RVDS/ARM_CM4F/port.c 

# Add BSP SOURCE FILES TO COMPLE HERE
BSP_SRC_FILES= \
	STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_rcc.c \
	STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_rcc_ex.c \
	STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_cortex.c \
	STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_gpio.c \
	STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_pwr_ex.c \
	STM32L4xx_HAL_Driver/Src/stm32l4xx_hal.c \
	CMSIS/Device/ST/STM32L4xx/Source/Templates/gcc/startup_stm32l476xx.s

# ADD APPLICATION SOURCE FILES TO COMPILE HERE
APP_SRC_FILES= \
	       app/src/system_stm32l4xx.c \
	       app/src/stm32l4xx_it.c \
	       app/src/main.c

# Dont edit this line
include build/main.mk
include build/debug.mk
