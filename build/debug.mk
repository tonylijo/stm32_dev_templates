OPENOCD?=openocd -s /usr/local/share/openocd/scripts/ -f board/stm32l4discovery.cfg
.PHONY: gdb
gdb: $(TARGET_EXECUTABLE).elf
	$(OPENOCD) -c \
		"init; \
		reset halt \
		" &
	$(CROSS_COMPILE)gdb  
